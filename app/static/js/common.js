function getSearchParams(k) {
	var p = {};
	location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(s, k, v) {
		p[k] = v
	})
	return k ? p[k] : p;
}

var clickDelay = 500,
	clickDelayTimer = null;
var video_part1 =  '<iframe src="https://player.vimeo.com/video/',
	video_part2 = '?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen">';

function addScript(src) {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = src;
	document.body.appendChild(script);
}
addScript("https://player.vimeo.com/api/player.js");
$('.burger-click-region').on('click', function() {

	if (clickDelayTimer === null) {

		var $burger = $('.burger-menu-piece');
		$burger.toggleClass('active');
		$('.burger-menu-txt').toggleClass('is-open');

		if (!$burger.hasClass('active')) {
			$burger.addClass('closing');
		}

		clickDelayTimer = setTimeout(function() {
			$burger.removeClass('closing');
			clearTimeout(clickDelayTimer);
			clickDelayTimer = null;
		}, clickDelay);
	}

});

$(".st0").hover(
	function() {
		$(".vimeo-wrapper").addClass('off');
	},
	function() {
		$(".vimeo-wrapper").removeClass('off');
	}
);