$.fancybox.defaults.animationEffect = "zoom";
// global variables
var ScreenWidth = $(window).width();
function preloaderOff() {
	 $('.loaderArea').delay(10).fadeOut(1000);
}
$('#feed').submit(function(e) {
	e.preventDefault();
	$.post($(this).attr('action'), $(this).serialize()).done(function(data) {
		var feed = $('#feed'); 
		if (data == '1') {
			$(feed).find('.row .col-lg-4').hide();
			$(feed).find('.feedback__button').hide();
			$(feed).find('.error-message').addClass('d-none');
			$(feed).find('.feedback_thank').removeClass('d-none');
		}else{
			$(feed).find('.error-message').removeClass('d-none');
			$(feed).find('input').addClass('error');
		}
	});
})

$('#van_feed').submit(function(e) {
	e.preventDefault();
	$.post($(this).attr('action'), $(this).serialize()).done(function(data) {
		var feed = $('#van_feed'); 
		if (data == '1') {
			$(feed).find('.form-control').hide();
			$(feed).find('.van-Gogh__feedback-button').hide();
			$(feed).find('.error-message').addClass('d-none');
			console.log($('.van-Gogh__title'));
			$('.van-Gogh__title').addClass('d-none');
			$(feed).find('.feedback_thank').removeClass('d-none');
			window.setTimeout(function(){
				$('.van-Gogh').removeClass('active');
				window.setTimeout(function(){
					$('.van-Gogh').addClass('d-none');
				},3000)
			},5000);
		}else{
			$(feed).find('.error-message').removeClass('d-none');
			$(feed).find('input').addClass('error');
		}
	});
})
// sporretimur@gmail.com
// +77074747910
//Timenty