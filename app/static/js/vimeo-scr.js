function vimeoCreate($this ,id ,height, width){
	var options =
	{
		id: id,
		width: width,
		height: height,
	}
	var play = new Vimeo.Player($this, options);
	$($this).removeClass('playIco');
}
$(document).ready(function() {
	if ($('.video-fbox').length) {
		// addScript("https://player.vimeo.com/api/player.js");
		function getVimeoInf(blocks, callback){
		  var result = 0;
		  $(blocks).each(function(){
		  	var href = $(this).attr('href');
		  	var match = /vimeo.*\/(\d+)/i.exec(href);
		  	if (match) {
		  		var vimeoVideoID = match[1];
		  		var $this = this;
		  		console.log(vimeoVideoID);
                $.getJSON('https://www.vimeo.com/api/v2/video/' + vimeoVideoID + '.json?callback=?', { format: "json" }, function (data) {
                    featuredImg = data[0].thumbnail_large;
                    $($this).addClass('playIco').parent('div').css("background-image",'url("'+featuredImg+'")');
                    result = 1;
                });
                // window.setTimeout(vimeoCreate($this, vimeoVideoID, $($this).height(), $($this).width()),100)
		  	}
		  });
		  // $(block).height(maxHeight);
		}
		window.setTimeout(function(){
			getVimeoInf($('.video-fbox'));
		},0);
	}
});