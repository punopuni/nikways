// Велосипед Тиментия
	// 1 - Родительский блок
	// 2 - Управляющие кнопки
	// 3 - Угнетаемые блоки
	// 4 - Добавляемый класс
	// Не подразумеваю об ошибках
	// никаких проверок на ошибки
		// parent: '#expirience',
		// master: '.experience-section__block',
		// slaves: '.experience-content__blocks',
		// addedClass: 'active',
		// dataAttr: 'exp',

function blockTogglerForeach($parameters) {


	$.each($($parameters.parent), function($elemi , $elem){
		try {
			$($elem).find($parameters.master).click(function($this) {
				console.log($this);
				$($elem).find($parameters.master).removeClass($parameters.addedClass);
				$(this).addClass($parameters.addedClass);
				$($elem).find($parameters.slaves).removeClass($parameters.addedClass);
				var data = $(this).data($parameters.dataAttr);
				var strGen = $parameters.slaves + '[data-' + $parameters.dataAttr + '="' + data + '"]';
				$($elem).find(strGen).addClass($parameters.addedClass);
			});
		} catch (e) {
			console.log('blockToggler: error');
		}
		
	});
}