$('.project-slider').slick({
  centerMode: true,
  centerPadding: '560px',
  slidesToShow: 1,
  prevArrow: '#project-slider__prev',
  nextArrow: '#project-slider__next',
  focusOnSelect: true,
  responsive: [
	{
	  breakpoint: 1200,
	  settings: {
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    dots: false,
  		centerMode: false
	  }
	},
	{
	  breakpoint: 1400,
	  settings: {
  		centerPadding: '300px' 
	  }
	},
	{
	  breakpoint: 1700,
	  settings: {
  		centerPadding: '400px' 
	  }
	},
	]
});
if ($('.services-slider').length) {
	var currentSlide = 0;
	// getSearchParams('id');
	var getCurrent = getSearchParams('id');
	if (getCurrent) {
		currentSlide = $('.current_slide').data('slick');
	}
	$('.services-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		initialSlide: currentSlide,
		prevArrow: '.services-slider__prev',
	  	nextArrow: '.services-slider__next',
	  	arrows: true
	});
	blockTogglerForeach({
		parent: '.services-inside__slide',
		master: '.services-inside__nav',
		slaves: '.services-inside__desc',
		addedClass: 'active',
		dataAttr: 'id',
	});
}